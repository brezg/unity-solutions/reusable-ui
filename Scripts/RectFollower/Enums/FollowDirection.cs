namespace Brezg.UI
{
	public enum FollowDirection
	{
		Center,
		Left,
		Right,
		Top,
		Bottom,
	}
}
