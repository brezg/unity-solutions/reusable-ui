﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif


namespace Brezg.UI
{
	public class GraphicsTargetRouter : Graphic
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[SceneObjectsOnly, Required]
		#endif
		private List<Graphic> _targetGraphics = new List<Graphic>();


		public override void CrossFadeColor(Color targetColor, float duration, bool ignoreTimeScale, bool useAlpha)
		{
			foreach (var targetGraphic in _targetGraphics)
				targetGraphic.CrossFadeColor(targetColor, duration, ignoreTimeScale, useAlpha, true);
		}
	}
}
