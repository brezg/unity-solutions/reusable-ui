﻿using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif


namespace Brezg.Utils
{
	[RequireComponent(typeof(CanvasGroup))]
	public class CanvasGroupAsync : MonoBehaviour
	{
		[SerializeField]
		private bool _blocksRaycastWhenVisible = true;


		[SerializeField]
		private bool _blocksRaycastWhenHidden;


		private CanvasGroup _canvasGroup;


		public float Alpha
		{
			get => _canvasGroup.alpha;

			set
			{
				if (_canvasGroup == null)
					_canvasGroup = GetComponent<CanvasGroup>();

				_canvasGroup.alpha = value;

				UpdateBlockRaycast();
			}
		}


		public bool BlocksRaycasts
		{
			get
			{
				if (IsHidden)
					return _blocksRaycastWhenHidden;

				return _blocksRaycastWhenVisible;
			}
		}


		public bool IsHidden => Alpha <= 0.01;


		public bool IsShown => Alpha >= 0.99;
		


		private void Awake()
		{
			_canvasGroup = GetComponent<CanvasGroup>();

			UpdateBlockRaycast();
		}



		private void UpdateBlockRaycast()
		{
			_canvasGroup.blocksRaycasts = BlocksRaycasts;
		}


		#if ODIN_INSPECTOR
		[Button]
		#endif
		public void Show()
			=> Alpha = 1;


		public async UniTask ShowAsync(int milliseconds)
			=> await FadeAsync(milliseconds, true);


		public async UniTask ShowAsync(int milliseconds, CancellationToken token)
			=> await FadeAsync(milliseconds, true, token).SuppressCancellationThrow();


		#if ODIN_INSPECTOR
		[Button]
		#endif
		public void Hide()
			=> Alpha = 0;


		public async UniTask HideAsync(int milliseconds)
			=> await FadeAsync(milliseconds, false);


		public async UniTask HideAsync(int milliseconds, CancellationToken token)
			=> await FadeAsync(milliseconds, false, token).SuppressCancellationThrow();



		private async UniTask FadeAsync(int milliseconds, bool isIn) 
			=> await FadeAsync(milliseconds, isIn, CancellationToken.None).SuppressCancellationThrow();


		private async UniTask FadeAsync(int milliseconds, bool isIn, CancellationToken token)
		{
			if (!isIn && IsHidden || isIn && IsShown)
				return;

			_canvasGroup.alpha = isIn ? 0f : 1f;

			var tween = DOTween.To(() => _canvasGroup.alpha, f => _canvasGroup.alpha = f, isIn ? 1f : 0f, milliseconds / 1000f);

			await UniTask.Delay(milliseconds, false, PlayerLoopTiming.Update, token);

			tween.Complete();
			UpdateBlockRaycast();
		}
	}
}
