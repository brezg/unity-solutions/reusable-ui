using UnityEngine;
using UnityEngine.UI;
using Axis = UnityEngine.RectTransform.Axis;


namespace Brezg.UI
{
	[RequireComponent(typeof(Image))]
	[RequireComponent(typeof(RectTransform))]
	[ExecuteAlways]
	public class SliceMultiplierSizer : MonoBehaviour
	{
		[SerializeField]
		private Axis _axis = Axis.Vertical;
		
		[SerializeField]
		private float _sizeMultiplier = 1f;

		private RectTransform _selfRectTransform;

		private Image _selfImage;

		
		private void Awake()
		{
			_selfRectTransform = GetComponent<RectTransform>();
			_selfImage = GetComponent<Image>();
		}


		private void Update()
		{
			#if UNITY_EDITOR
			if (_selfRectTransform is null || _selfImage is null)
				Awake();
			#endif
			
			if (_selfImage.type != Image.Type.Sliced)
			{
				Debug.LogWarning("Wrong image type");
				return;
			}

			var size = _axis == Axis.Vertical ? _selfRectTransform.rect.height : _selfRectTransform.rect.width;

			_selfImage.pixelsPerUnitMultiplier = _sizeMultiplier / size;
		}
	}
}
