﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;


namespace Brezg.UI
{
	public class ClickAwaiter : MonoBehaviour
	{
		private bool _isClicked;


		public void _OnClick()
		{
			_isClicked = true;
		}
		
		
		public async UniTask WaitForClick(CancellationToken? token = null)
		{
		    _isClicked = false;

		    if (token.HasValue)
			    await UniTask.WaitUntil(() => _isClicked, cancellationToken: token.Value);
		    else
			    await UniTask.WaitUntil(() => _isClicked);
		}
	}
}
